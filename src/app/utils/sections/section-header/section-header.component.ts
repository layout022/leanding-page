import { Component } from '@angular/core';
import { NavbarHeaderComponent } from "../../navbar/navbar-header/navbar-header.component";

@Component({
    selector: 'app-section-header',
    standalone: true,
    templateUrl: './section-header.component.html',
    styleUrl: './section-header.component.css',
    imports: [NavbarHeaderComponent]
})
export class SectionHeaderComponent {

}
